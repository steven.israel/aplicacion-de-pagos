<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'tipo_persona'], function() {
    Route::get('/', 'TipoPersonaController@index');
    Route::get('/crear', 'TipoPersonaController@create');
	Route::get('/edit/{persona}', 'TipoPersonaController@edit');
	Route::get('/show/{persona}', 'TipoPersonaController@show');
    Route::post('/crear', 'TipoPersonaController@store');
    Route::get('/find/{busqueda}', 'TipoPersonaController@find');
    Route::put('/update/{persona}', 'TipoPersonaController@update');
    Route::get('/anular/{persona}', 'TipoPersonaController@destroy');
});

Route::group(['prefix' => 'tipo_cuenta'], function() {
    Route::get('/', 'TipoCuentaController@index');
    Route::get('/crear', 'TipoCuentaController@create');
	Route::get('/edit/{cuenta}', 'TipoCuentaController@edit');
	Route::get('/show/{cuenta}', 'TipoCuentaController@show');
    Route::post('/crear', 'TipoCuentaController@store');
    Route::get('/find/{busqueda}', 'TipoCuentaController@find');
    Route::put('/update/{cuenta}', 'TipoCuentaController@update');
    Route::get('/anular/{cuenta}', 'TipoCuentaController@destroy');
});


Route::get('/cat_tipo_contribuyente', 'CatTipoContribuyenteController@index')->name('cat_tipo_contribuyente');
