@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Vista tipo de contribuyente</h2>

        <ul class="nav navbar-right panel_toolbox">
          <li><a class="close-link" href="{{ url('tipo_persona') }}"><i class="fa fa-long-arrow-left"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        {!! Form::open(['class' => 'form-horizontal form-label-left input_mask']) !!}

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          {!! Form::text('codigo_tipo_contribuyente',$persona->codigo_tipo_contribuyente, ['class'=>'form-control has-feedback-left', 'placeholder'=>'Codigo','disabled'=> true]) !!}
          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
          <p style="color: red;">{{ $errors->first('codigo_tipo_contribuyente') }}</p>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          {!! Form::select('estado', array('1' => 'Activo', '0' => 'Inactivo'), $persona->estado,['class'=>'form-control','disabled'=> true]) !!}
          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
          <p style="color: red;">{{ $errors->first('estado') }}</p>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
          {!! Form::textarea('descripcion',$persona->descripcion, ['class'=>'form-control has-feedback-left','disabled'=> true, 'placeholder'=>'Descripción']) !!}
          <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
          <p style="color: red;">{{ $errors->first('descripcion') }}</p>
        </div>
        <div class="col-md-12">
           <div class="form-group">
              <a class="close-link" href="{{ url('tipo_persona') }}">
                <button type="button" class="btn btn-primary">Cancel</button>
              </a>
          </div>
        </div>
       
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection