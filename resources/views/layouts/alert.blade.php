<div class="flash-message">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
	@if(Session::has('alert-' . $msg))
	<div class="alert alert-{{ $msg }} alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
		</button>
		<strong>{{ Session::get('alert-' . $msg) }}</strong>.
	</div>
	@endif
	@endforeach
</div>