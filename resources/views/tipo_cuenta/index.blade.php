@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">

			<!-- incluimos la vista de mesaje de alerta -->
			@include('layouts/alert')
			<div class="x_title">
				<h2>Tipo cuenta</h2>

				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<div class="pull-left">
					<a class="btn btn-app" href="{{ url('tipo_cuenta/crear') }}">
						<i class="fa fa-plus"></i> 
						Nuevo
					</a>
				</div>
				<div class="title_right">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="input-group">
							<input type="text" id="search" class="form-control" onkeyup="get_tipo_cuenta()" placeholder="Buscador">
							<span class="input-group-btn">
								<a href="/tipo_cuenta"><button class="btn btn-default" type="button" >Todos!</button></a>
							</span>
						</div>
					</div>
				</div>
			</div>
			Total de registros: {{ $tipo_cuentas->total() }}
			<table id="datatable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>Codigo</th>
						<th>Descripción</th>
						<th>Estado</th>
						<th>Creación</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody id="set_items">
					@foreach($tipo_cuentas as $tipo_cuenta)
					<tr>
						<td>{{ $tipo_cuenta->id_categoria_cuenta }}</td>
						<td>{{ $tipo_cuenta->codigo_tipo_cuenta }}</td>
						<td>{{ $tipo_cuenta->descripcion }}</td>
						<td>{{ auth()->user()->estado($tipo_cuenta->estado) }}</td>
						<td>{{ $tipo_cuenta->created_at }}</td>
						<td width="10%">
							<a href="{{ url('tipo_cuenta/edit',['cuenta' => $tipo_cuenta->id_categoria_cuenta]) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"><i style="font-size: 20px; font-weight: bold;" class="fa fa-edit"></i></a>
							<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver" href="{{ url('tipo_cuenta/show',['cuenta' => $tipo_cuenta->id_categoria_cuenta]) }}"><i style="font-size: 20px; font-weight: bold; color: green;" class="fa fa-eye"></i></a>
							<a style="cursor: pointer;" data-toggle="modal" data-target=".bs-example-modal-sm{{ $tipo_cuenta->id_categoria_cuenta }}">
								<i data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar" style="font-size: 20px; font-weight: bold; color: red;" class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
					<div class="modal fade bs-example-modal-sm{{ $tipo_cuenta->id_categoria_cuenta }}" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">

								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
									</button>
									<h4 class="modal-title" id="myModalLabel2">{{$tipo_cuenta->descripcion}}</h4>
								</div>
								<div class="modal-body">
									<h4>Desea eliminar este item?</h4>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
									<a href="{{ url('tipo_cuenta/anular',['cuenta' => $tipo_cuenta->id_categoria_cuenta]) }}">
										<button type="button" class="btn btn-primary">Eliminar</button>
									</a>
								</div>

							</div>
						</div>
					</div>
					@endforeach
				</tbody>
			</table>
			{{ $tipo_cuentas->appends(request()->input())->links() }}
		</div>
	</div>
</div>
</div>
<button type="button" class="btn btn-primary" >Small modal</button>


<script>
	function get_tipo_cuenta(){
		//console.log($("#autor").val());
		if($("#search").length === 0 || !$("#search").val().trim()){
			$.server("{{ url('tipo_cuenta/find/') }}/"+1,
				null,
				function(data){
					if(data.type=='success'){
						$('#set_items').empty();
						Object.entries(data.items).forEach(([key, value]) => {
							var estado = "";
							if (value['estado'] == 1) {
								estado = "Activo";
							}else{
								estado = "Inactivo";
							}
							$('#set_items').append('<tr>'+
								'<td>'+value['id_categoria_cuenta']+'</td>'+
								'<td>'+value['codigo_tipo_cuenta']+'</td>'+
								'<td>'+value['descripcion']+'</td>'+
								'<td>'+
								estado
								+'</td>'+
								'<td>'+value['created_at']+'</td>'+
								'<td width="10%">'+

								'<a href="/tipo_cuenta/edit/'+value['id_categoria_cuenta']+'">'+
								'<i style="font-size: 20px; font-weight: bold;" class="fa fa-edit"></i></a>'+
								'<a href=""><i style="font-size: 20px; font-weight: bold; color: green;" class="fa fa-eye"></i></a>'+
								'<a href=""><i style="font-size: 20px; font-weight: bold; color: red;" class="fa fa-trash"></i></a>'+
								'</td>'+
								'</tr>');
						});
					}else{
						$('#set_items').empty();
						$('#set_items').append('<p>No se encontraron resultados.</p>');
					}
				},
				'get',
				'json'
				);
		}else{
			$.server("{{ url('tipo_cuenta/find/') }}/"+$("#search").val(),
				null,
				function(data){
					if(data.type=='success'){
						$('#set_items').empty();
						Object.entries(data.items).forEach(([key, value]) => {
							var estado = "";
							if (value['estado'] == 1) {
								estado = "Activo";
							}else{
								estado = "Inactivo";
							}
							$('#set_items').append('<tr>'+
								'<td>'+value['id_categoria_cuenta']+'</td>'+
								'<td>'+value['codigo_tipo_cuenta']+'</td>'+
								'<td>'+value['descripcion']+'</td>'+
								'<td>'+
								estado
								+'</td>'+
								'<td>'+value['created_at']+'</td>'+
								'<td width="10%">'+
								'<a href="/tipo_cuenta/edit/'+value['idtipo_contribuyente']+'">'+
								'<i style="font-size: 20px; font-weight: bold;" class="fa fa-edit"></i></a>'+
								'<a href=""><i style="font-size: 20px; font-weight: bold; color: green;" class="fa fa-eye"></i></a>'+
								'<a href=""><i style="font-size: 20px; font-weight: bold; color: red;" class="fa fa-trash"></i></a>'+
								'</td>'+
								'</tr>');
						});
					}else{
						$('#set_items').empty();
						$('#set_items').append('<p>No se encontraron resultados.</p>');
					}
				},
				'get',
				'json'
				);
		}
	}
</script>
@endsection