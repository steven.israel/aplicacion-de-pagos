@extends('layouts.app')

@section('content')

<div class="row">

  <div class="col-md-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>EDITAR tipo de cuenta</h2>

        <ul class="nav navbar-right panel_toolbox">
          <li><a class="close-link" href="{{ url('tipo_tcuenta') }}"><i class="fa fa-long-arrow-left"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <strong>Error!</strong> Revise los campos obligatorios.
        </div>
      @endif
      <div class="x_content">
        <br />
        {!! Form::open([ 'url'=> [ '/tipo_cuenta/update', 'tcuenta' => $tcuenta->id_categoria_cuenta ], 'method'=>'put', 'class' => 'form-horizontal form-label-left input_mask']) !!}

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          {!! Form::text('codigo_tipo_cuenta',$tcuenta->codigo_tipo_cuenta, ['class'=>'form-control has-feedback-left', 'placeholder'=>'Codigo','disabled'=> true]) !!}
          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
          <p style="color: red;">{{ $errors->first('codigo_tipo_cuenta') }}</p>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
          {!! Form::select('estado', array('1' => 'Activo', '0' => 'Inactivo'), $tcuenta->estado,['class'=>'form-control']) !!}
          <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
          <p style="color: red;">{{ $errors->first('estado') }}</p>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
          {!! Form::textarea('descripcion',$tcuenta->descripcion, ['class'=>'form-control has-feedback-left', 'placeholder'=>'Descripción']) !!}
          <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
          <p style="color: red;">{{ $errors->first('descripcion') }}</p>
        </div>
        <div class="col-md-12">
           <div class="form-group">
              <a class="close-link" href="{{ url('tipo_cuenta') }}">
                <button type="button" class="btn btn-primary">Cancel</button>
              </a>
              <button type="submit" class="btn btn-success">Actualizar</button>
          </div>
        </div>
       
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection