<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class detalle_tributos_cuenta extends Model
{
    protected $table = "detalle_tributos_cuenta";

    protected $primaryKey = 'id_detalle_tributos_cuenta';

    protected $fillable = ['id_cuenta','id_codigo_tipo_tributo','id_subtipo_tributo','aplica_minimo_maximo','estado','created_at','updated_at'];

     public function cuenta()
    {
        return $this->hasOne('App\Models\cuentas');
    }
 	public function tipo_tributo()
    {
        return $this->hasOne('App\Models\cat_tipo_tributos');
    }  
 	public function subtipo_tributo()
    {
        return $this->hasOne('App\Models\cat_subtipo_tributos');
    }  
}
