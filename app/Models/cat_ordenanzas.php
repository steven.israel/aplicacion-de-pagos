<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cat_ordenanzas extends Model
{
    protected $table = "cat_ordenanzas";

    protected $primaryKey = 'id_codigo_ordenanza';

    protected $fillable = ['codigo_ordenanza','descripcion','fecha_vigencia_inicial','fecha_vigencia_final','estado','created_at','updated_at'];

}
