<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cat_origen_pago extends Model
{
    protected $table = "cat_origen_pago";

    protected $primaryKey = 'id_cat_origen_pago';

    protected $fillable = ['codigo_origen_pago', 'descripcion', 'estado','created_at','updated_at'];

}
