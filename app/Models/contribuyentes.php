<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class contribuyentes extends Model
{
    protected $table = "contribuyentes";

    protected $primaryKey = 'id_contribuyente';

    protected $fillable = ['dui','nit','primer_apellido','segundo_apellido','primer_nombre','segundo_nombre','direccion','telefonos','correo_electronico','idtipo_contribuyente','estado','created_at','updated_at'];

     public function tipoContribuyente()
    {
        return $this->hasOne('App\Models\cat_tipo_contribuyente');
    }
   
}
