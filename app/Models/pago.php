<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pago extends Model
{
    protected $table = "pagos";

    protected $primaryKey = 'id';

    protected $fillable = ['codigo_pagos','id_detalle_facturacion','id_codigo_tipo_tributo','id_subtipo_tributo','id_cat_origen_pago', 'valor_facturado', 'valor_paga_abono', 'fecha_pago', 'fecha_aplica', 'usuario', 'estado','created_at','updated_at'];


 	public function tipoTributo()
    {
        return $this->hasOne('App\Models\cat_tipo_tributos');
    }  

 	public function subTipoTributo()
    {
        return $this->hasOne('App\Models\cat_subtipo_tributos');
    }  
 	public function detalleFactura()
    {
        return $this->hasOne('App\Models\detalle_facturacion');
    }  
 	public function origen()
    {
        return $this->hasOne('App\Models\cat_origen_pago');
    }  


    
}
