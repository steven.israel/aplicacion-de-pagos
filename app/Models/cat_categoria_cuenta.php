<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cat_categoria_cuenta extends Model
{
    protected $table = "cat_categoria_cuenta";

    protected $primaryKey = 'id_categoria_cuenta';

    protected $fillable = ['codigo_tipo_cuenta','descripcion','estado','created_at','updated_at'];
}
