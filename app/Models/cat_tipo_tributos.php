<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cat_tipo_tributos extends Model
{
    protected $table = "cat_tipo_tributos";

    protected $primaryKey = 'id_codigo_tipo_tributo';

    protected $fillable = ['codigo_tipo_tributo','id_categoria_cuenta','descripcion','estado','created_at','updated_at'];

    public function tipoCuenta()
    {
        return $this->hasOne('App\Models\cat_categoria_cuenta');
    }
}
