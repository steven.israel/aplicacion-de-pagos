<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cat_subtipo_tributos extends Model
{
    protected $table = "cat_subtipo_tributos";

    protected $primaryKey = 'id_subtipo_tributo';

    protected $fillable = ['cat_subtipo_tributo','id_codigo_tipo_tributo','id_codigo_ordenanza','descripcion','valor_minimo','valor_maximo','estado','created_at','updated_at'];

     public function tipoTributo()
    {
        return $this->hasOne('App\Models\cat_tipo_tributos');
    }
     public function Ordenanza()
    {
        return $this->hasOne('App\Models\cat_ordenanzas');
    }
}
