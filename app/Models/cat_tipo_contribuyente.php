<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cat_tipo_contribuyente extends Model
{
    protected $table = "cat_tipo_contribuyente";

    protected $primaryKey = 'idtipo_contribuyente';

    protected $fillable = ['codigo_tipo_contribuyente','descripcion','estado','created_at','updated_at'];

}
