<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class facturacion extends Model
{
    protected $table = "facturacion";

    protected $primaryKey = 'id_facturacion';

    protected $fillable = ['codigo_factura','id_contribuyente','id_cuenta','total_facturado','periodo','estado','created_at','updated_at'];


 	public function contribuyente()
    {
        return $this->hasOne('App\Models\contribuyentes');
    }  
 
    public function cuenta()
    {
        return $this->hasOne('App\Models\cuentas');
    }  

}
