<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cuentas extends Model
{
    protected $table = "cuentas";

    protected $primaryKey = 'id_cuenta';

    protected $fillable = ['codigo_cuenta','id_contribuyente','id_categoria_cuenta','fecha_creacion','estado','created_at','updated_at'];

     public function Contribuyente()
    {
        return $this->hasOne('App\Models\contribuyentes');
    }
 	public function categoria_cuenta()
    {
        return $this->hasOne('App\Models\cat_categoria_cuenta');
    }    
}
