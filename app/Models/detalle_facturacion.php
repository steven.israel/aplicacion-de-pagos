<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class detalle_facturacion extends Model
{
    protected $table = "detalle_facturacion";

    protected $primaryKey = 'id_detalle_facturacion';

    protected $fillable = ['codigo_detalle_factura','id_facturacion','id_cuenta','id_codigo_tipo_tributo', 'id_subtipo_tributo', 'valor_facturado', 'valor_pagado', 'periodo', 'estado','created_at','updated_at'];

    public function facturacion()
    {
        return $this->hasOne('App\Models\facturacion');
    }

    public function cuentas()
    {
        return $this->hasOne('App\Models\cuentas');
    }

    public function cat_tipo_tributos()
    {
        return $this->hasOne('App\Models\cat_tipo_tributos');
    }

    public function cat_subtipo_tributos()
    {
        return $this->hasOne('App\Models\cat_subtipo_tributos');
    }

    
}
