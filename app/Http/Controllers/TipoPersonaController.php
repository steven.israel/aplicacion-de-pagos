<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Models\cat_tipo_contribuyente;

class TipoPersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tipo_personas'] = cat_tipo_contribuyente::orderBy('created_at','DESC')->paginate(5);

        //return $data;

        return view('tipo_persona.index',$data);
    }

    public function find($busqueda)
    {
        if ($busqueda == 1) {
            $items = cat_tipo_contribuyente::orderBy('created_at','DESC')->take(5)->get();

            return response()->json(['type'=>'success','items' => $items], 200);
        }else{
            $items = cat_tipo_contribuyente::where('descripcion','like','%'.$busqueda.'%')->orWhere('codigo_tipo_contribuyente','like','%'.$busqueda.'%')->orWhere('created_at','like','%'.$busqueda.'%')->orderBy('created_at','DESC')->get();

            return response()->json(['type'=>'success','items' => $items], 200);
        }

        return response()->json(['type'=>'error'], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo_persona.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'codigo_tipo_contribuyente' => 'required|string|max:2|unique:cat_tipo_contribuyente',
            'estado'                    => 'required|integer|min:0|max:1',
            'descripcion'               => 'required|string|max:10',
        ]);

        DB::transaction(function () use($request) {
            $persona = new cat_tipo_contribuyente();
            $persona->codigo_tipo_contribuyente = $request->codigo_tipo_contribuyente;
            $persona->descripcion               = $request->descripcion;
            $persona->estado                    = $request->estado;
            $persona->save();
        });

        
        $request->session()->flash('alert-success', 'Registro ingresado exitosamente');
        return redirect('tipo_persona');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(cat_tipo_contribuyente $persona)
    {
        return view('tipo_persona.show',['persona' => $persona]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(cat_tipo_contribuyente $persona)
    {
        return view('tipo_persona.edit',['persona' => $persona]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,cat_tipo_contribuyente $persona)
    {
        // $validator = Validator::make($request->all(), [
        //     'estado'                    => 'required|integer|min:0|max:1',
        //     'descripcion'               => 'required|string|max:10',
        // ]);

        // if ($validator->fails()) return redirect('tipo_persona/edit/'.$persona->idtipo_contribuyente)->withErrors($validator->errors()) ;

        $this->validate($request, [
            'estado'                    => 'required|integer|min:0|max:1',
            'descripcion'               => 'required|string|max:10',
        ]);

        DB::transaction(function () use($request, $persona) {
            $persona->descripcion               = $request->descripcion;
            $persona->estado                    = $request->estado;
            $persona->save();
        });
        
        $request->session()->flash('alert-success', 'Registro actualizado exitosamente');
        return redirect('tipo_persona');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,cat_tipo_contribuyente $persona)
    {
        $persona->delete();

        $request->session()->flash('alert-success', 'Registro eliminado exitosamente');
        return redirect('tipo_persona');
    }
}
