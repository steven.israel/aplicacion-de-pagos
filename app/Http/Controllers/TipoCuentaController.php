<?php

namespace App\Http\Controllers;
use DB;
use Validator;
use Illuminate\Http\Request;
use App\Models\cat_categoria_cuenta;

class TipoCuentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['tipo_cuentas'] = cat_categoria_cuenta::orderBy('created_at','DESC')->paginate(5);

        //return $data;

        return view('tipo_cuenta.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo_cuenta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'codigo_tipo_cuenta'        => 'required|string|max:4|unique:cat_categoria_cuenta',
            'estado'                    => 'required|integer|min:0|max:1',
            'descripcion'               => 'required|string|max:50',
        ]);

        DB::transaction(function () use($request) {
            $tcuenta = new cat_categoria_cuenta();
            $tcuenta->codigo_tipo_cuenta        = $request->codigo_tipo_cuenta;
            $tcuenta->descripcion               = $request->descripcion;
            $tcuenta->estado                    = $request->estado;
            $tcuenta->save();
        });

       
        $request->session()->flash('alert-success', 'Registro ingresado exitosamente');
        return redirect('tipo_cuenta');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(cat_categoria_cuenta $cuenta)
    {
        return view('tipo_cuenta.show',['tcuenta' => $cuenta]);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(cat_categoria_cuenta $cuenta)
    {
        return view('tipo_cuenta.edit',['tcuenta' => $cuenta]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cat_categoria_cuenta $cuenta)
    {
        $this->validate($request, [
            'estado'                    => 'required|integer|min:0|max:1',
            'descripcion'               => 'required|string|max:50',
        ]);

        DB::transaction(function () use($request, $cuenta) {
            $cuenta->descripcion               = $request->descripcion;
            $cuenta->estado                    = $request->estado;
            $cuenta->save();
        });
        
        $request->session()->flash('alert-success', 'Registro actualizado exitosamente');
        return redirect('tipo_cuenta');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, cat_categoria_cuenta $cuenta)
    {
        $cuenta->delete();

        $request->session()->flash('alert-success', 'Registro eliminado exitosamente');
        return redirect('tipo_cuenta');
    }
}
