<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatTipoTributosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_tipo_tributos', function (Blueprint $table) {
            $table->bigIncrements('id_codigo_tipo_tributo');
            $table->char('codigo_tipo_tributo',5)->unique();
            $table->unsignedBigInteger('id_categoria_cuenta');
            $table->String('descripcion',100);
            $table->boolean('estado')->default(1); 
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('id_categoria_cuenta')->references('id_categoria_cuenta')->on('cat_categoria_cuenta');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_tipo_tributos');
    }
}
