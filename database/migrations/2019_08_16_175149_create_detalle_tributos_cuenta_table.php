<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleTributosCuentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_tributos_cuenta', function (Blueprint $table) {
            $table->bigIncrements('id_detalle_tributos_cuenta');
            $table->unsignedBigInteger('id_cuenta');
            $table->unsignedBigInteger('id_codigo_tipo_tributo');
            $table->unsignedBigInteger('id_subtipo_tributo');
            $table->char('aplica_minimo_maximo',2);
            $table->boolean('estado')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('id_cuenta')->references('id_cuenta')->on('cuentas');
            $table->foreign('id_codigo_tipo_tributo')->references('id_codigo_tipo_tributo')->on('cat_tipo_tributos');
            $table->foreign('id_subtipo_tributo')->references('id_subtipo_tributo')->on('cat_subtipo_tributos');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_tributos_cuenta');
    }
}
