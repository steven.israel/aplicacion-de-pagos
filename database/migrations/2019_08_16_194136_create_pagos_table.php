<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('codigo_pagos',10)->unique();        
            $table->unsignedBigInteger('id_detalle_facturacion');
            $table->unsignedBigInteger('id_codigo_tipo_tributo');
            $table->unsignedBigInteger('id_subtipo_tributo');
            $table->unsignedBigInteger('id_cat_origen_pago');
            $table->decimal('valor_facturado', 18, 2);            
            $table->decimal('valor_paga_abono', 18, 2);            
            $table->datetime('fecha_pago');
            $table->datetime('fecha_aplica');
            $table->boolean('estado')->default(1);
            $table->string('usuario',25);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

        $table->foreign('id_detalle_facturacion')->references('id_detalle_facturacion')->on('detalle_facturacion');
        $table->foreign('id_codigo_tipo_tributo')->references('id_codigo_tipo_tributo')->on('cat_tipo_tributos');
        $table->foreign('id_subtipo_tributo')->references('id_subtipo_tributo')->on('cat_subtipo_tributos');
        $table->foreign('id_cat_origen_pago')->references('id_cat_origen_pago')->on('cat_origen_pago');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
