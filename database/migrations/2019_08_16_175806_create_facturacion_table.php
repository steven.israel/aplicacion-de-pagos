<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturacion', function (Blueprint $table) {
            $table->bigIncrements('id_facturacion');
            $table->char('codigo_factura',10)->unique();            
            $table->unsignedBigInteger('id_contribuyente');
            $table->unsignedBigInteger('id_cuenta');
            $table->decimal('total_facturado', 18, 2);            
            $table->char('periodo',6);
            $table->boolean('estado')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('id_contribuyente')->references('id_contribuyente')->on('contribuyentes');
            $table->foreign('id_cuenta')->references('id_cuenta')->on('cuentas');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturacion');
    }
}
