<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatOrdenanzasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_ordenanzas', function (Blueprint $table) {
            $table->bigIncrements('id_codigo_ordenanza');
            $table->char('codigo_ordenanza',5)->unique();
            $table->String('descripcion',55);
            $table->dateTime('fecha_vigencia_inicial');
            $table->dateTime('fecha_vigencia_final');
            $table->boolean('estado')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_ordenanzas');
    }
}
