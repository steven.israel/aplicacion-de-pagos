<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContribuyentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contribuyentes', function (Blueprint $table) {
            $table->bigIncrements('id_contribuyente');
            $table->char('dui',9)->unique();
            $table->char('nit',14);
            $table->string('primer_apellido',25);
            $table->string('segundo_apellido',25);
            $table->string('primer_nombre',25);
            $table->string('segundo_nombre',25);
            $table->string('direccion');
            $table->string('telefonos',50);
            $table->string('correo_electronico',50);
            $table->unsignedBigInteger('idtipo_contribuyente');
            $table->boolean('estado')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('idtipo_contribuyente')->references('idtipo_contribuyente')->on('cat_tipo_contribuyente');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contribuyentes');
    }
}
