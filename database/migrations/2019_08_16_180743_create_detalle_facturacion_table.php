<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleFacturacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_facturacion', function (Blueprint $table) {
            $table->bigIncrements('id_detalle_facturacion');
            $table->char('codigo_detalle_factura',10)->unique();            
            $table->unsignedBigInteger('id_facturacion');
            $table->unsignedBigInteger('id_cuenta');
            $table->unsignedBigInteger('id_codigo_tipo_tributo');
            $table->unsignedBigInteger('id_subtipo_tributo');
            $table->decimal('valor_facturado', 18, 2);            
            $table->decimal('valor_pagado', 18, 2);            
            $table->char('periodo',6);
            $table->boolean('estado')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('id_facturacion')->references('id_facturacion')->on('facturacion');
            $table->foreign('id_cuenta')->references('id_cuenta')->on('cuentas');    
            $table->foreign('id_codigo_tipo_tributo')->references('id_codigo_tipo_tributo')->on('cat_tipo_tributos');    
            $table->foreign('id_subtipo_tributo')->references('id_subtipo_tributo')->on('cat_subtipo_tributos');    

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_facturacion');
    }
}
