<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatSubtipoTributosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_subtipo_tributos', function (Blueprint $table) {
            $table->bigIncrements('id_subtipo_tributo');
            $table->char('cat_subtipo_tributo',5)->unique();
            $table->unsignedBigInteger('id_codigo_tipo_tributo');
            $table->unsignedBigInteger('id_codigo_ordenanza');
            $table->String('descripcion',50);
            $table->decimal('valor_minimo', 18, 2);
            $table->decimal('valor_maximo', 18, 2);
            $table->boolean('estado')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

        $table->foreign('id_codigo_tipo_tributo')->references('id_codigo_tipo_tributo')->on('cat_tipo_tributos');

        $table->foreign('id_codigo_ordenanza')->references('id_codigo_ordenanza')->on('cat_ordenanzas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_subtipo_tributos');
    }
}
