<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function (Blueprint $table) {
            $table->bigIncrements('id_cuenta');
            $table->char('codigo_cuenta',10)->unique();
            $table->unsignedBigInteger('id_contribuyente');
            $table->unsignedBigInteger('id_categoria_cuenta');
            $table->dateTime('fecha_creacion');
            $table->boolean('estado')->default(1);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('id_contribuyente')->references('id_contribuyente')->on('contribuyentes');
            $table->foreign('id_categoria_cuenta')->references('id_categoria_cuenta')->on('cat_categoria_cuenta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas');
    }
}
