<?php

use Illuminate\Database\Seeder;

class CatTipoContribuyenteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       DB::table('cat_tipo_contribuyente')->insert(
       	[
       		[
	            'codigo_tipo_contribuyente' => '01',
	            'descripcion' => 'NATURAL',
	            'estado' => true,
        	],
       		[
	            'codigo_tipo_contribuyente' => '02',
	            'descripcion' => 'JURIDICA',
	            'estado' => true,
        	]

    	]);
         
    }
}
