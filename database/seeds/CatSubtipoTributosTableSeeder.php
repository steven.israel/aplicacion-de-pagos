<?php

use Illuminate\Database\Seeder;

class CatSubtipoTributosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 		DB::table('cat_subtipo_tributos')->insert(
       	[
       		[
	            'cat_subtipo_tributo' => '00001',
	            'id_codigo_tipo_tributo' => 1,
	            'id_codigo_ordenanza' => 1,
	            'descripcion' => 'TIPO A',
	            'valor_minimo' => 5.5,
	            'valor_maximo' => 10.5,
        	],
        	[
	            'cat_subtipo_tributo' => '00002',
	            'id_codigo_tipo_tributo' => 1,
	            'id_codigo_ordenanza' => 1,
	            'descripcion' => 'TIPO B',
	            'valor_minimo' => 15.5,
	            'valor_maximo' => 110.5,
        	],
       		[
	            'cat_subtipo_tributo' => '00003',
	            'id_codigo_tipo_tributo' => 2,
	            'id_codigo_ordenanza' => 2,
	            'descripcion' => 'TIPO C',
	            'valor_minimo' => 2.5,
	            'valor_maximo' => 7.5,
        	],
        	[
	            'cat_subtipo_tributo' => '00004',
	            'id_codigo_tipo_tributo' => 2,
	            'id_codigo_ordenanza' => 2,
	            'descripcion' => 'TIPO D',
	            'valor_minimo' => 16.5,
	            'valor_maximo' => 100.5,
        	]
    	]);
    }
}
