<?php

use Illuminate\Database\Seeder;

class CatTipoTributosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       DB::table('cat_tipo_tributos')->insert(
       	[
       		[
	            'codigo_tipo_tributo' => '00001',
	            'id_categoria_cuenta' => 1,
	            'descripcion' => 'ALUMBRADO',
        	],
       		[
	            'codigo_tipo_tributo' => '00002',
	            'id_categoria_cuenta' => 1,
	            'descripcion' => 'ASEO',
        	],
       		[
	            'codigo_tipo_tributo' => '00003',
	            'id_categoria_cuenta' => 1,
	            'descripcion' => 'CONTRIBUCIÓN ESPECÍFICA',
        	],
       		[
	            'codigo_tipo_tributo' => '00004',
	            'id_categoria_cuenta' => 2,
	            'descripcion' => 'FCM',
        	],
       		[
	            'codigo_tipo_tributo' => '00005',
	            'id_categoria_cuenta' => 2,
	            'descripcion' => 'FEF',
        	],
       		[
	            'codigo_tipo_tributo' => '00006',
	            'id_categoria_cuenta' => 2,
	            'descripcion' => 'MULTAS',
        	],

    	]);
        
    }
}



