<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        	UsersTableSeeder::class,
        	CatTipoContribuyenteTableSeeder::class,
        	CatCategoriaCuentaTableSeeder::class,
        	CatTipoTributosTableSeeder::class,
        	CatOrdenanzasTableSeeder::class,
            CatSubtipoTributosTableSeeder::class,
        ]);
    }
}
