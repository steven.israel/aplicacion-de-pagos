<?php

use Illuminate\Database\Seeder;

class CatCategoriaCuentaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('cat_categoria_cuenta')->insert(
       	[
       		[
	            'codigo_tipo_cuenta' => '0001',
	            'descripcion' => 'INMUEBLE',
	            'estado' => true,
        	],
       		[
	            'codigo_tipo_cuenta' => '0002',
	            'descripcion' => 'IMPUESTOS',
	            'estado' => true,
        	]

    	]);
    }
}
