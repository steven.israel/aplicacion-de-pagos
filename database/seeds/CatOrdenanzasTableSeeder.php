<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class CatOrdenanzasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('cat_ordenanzas')->insert(
       	[
       		[
	            'codigo_ordenanza' => '00001',
	            'descripcion' => 'ORDENANZA 1',
	            'fecha_vigencia_inicial' => Carbon::now(),
	            'fecha_vigencia_final' => Carbon::now(),
	            
        	],
       		[
	            'codigo_ordenanza' => '00002',
	            'descripcion' => 'ORDENANZA 2',
	            'fecha_vigencia_inicial' => Carbon::now(),
	            'fecha_vigencia_final' => Carbon::now(),
        	]

    	]);
    }
}
