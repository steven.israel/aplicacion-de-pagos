URL repositorio proyecto: git clone https://gitlab.com/steven.israel/aplicacion-de-pagos.git
URL plantilla https://www.creative-tim.com/product/argon-dashboard

Comandos
1-comando para crear el proyecto composer -> create-project laravel/laravel nombre_proyecto
2-comando para copiar el archivo example -> cp .env.example .env
3-comando para ejecutar las migraciones -> php artisan migrate
4-comando para revertir las migraciones -> php artisan migrate:rollback
5-comando para crear seeders -> php artisan make:seeder NombreTableSeeder
6-comando para ejecutar seeders -> php artisan db:seed
7-comando para ejecutar un seeders en especifico -> php artisan db:seed --class=UsersTableSeeder
8-comando para ejecutar la migraciones y los seeders -> php artisan migrate:fresh --seed
9-codigo para crear las fechas por defecto de laravel dinamicamente -> 
		$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            	$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
10-comando para ejecutar el proyecto -> php artisan serve
11-comando para crear un modelo -> php artisan make:model nombreModelo
12-comando para crear el login por defecto de laravel -> php artisan make:auth
13-comando para instalar el paquete mix en la carpeta publica -> npm install
14-comando para crear las nuevas rutas de estilos -> npm run dev
15-comando para crear un controlador -> php artisan make:controller CatTipoContribuyenteController -r

Pasos para integrar una plantilla
1- copiar los estilos de la plantilla que vamos a utilizar
2- pegarla en la carpet resource
3- en la carpeta raiz abrir el archivo webpack
4- crear los grupos de estilos y scripts

	mix.styles([
    		'ruta de los estilosa utilizar'
	], 'public/css/nombreArchivoEstilo.css');

	mix.scripts([
		'ruta de los scripts'
	], 'public/js/nombreScript.js');

5- ejecutar el comando npm install
6- ejecutar el comando npm run dev para crear el archiv mix.manifest en la carpeta publica con las rutas de los estilo
7- en la vista referenciar los estilos y scripts a utilizar

	<link href="{{ asset('/css/all.css') }}" rel="stylesheet" />

Pasos para obtener los cambios del repositorio
1- git pull origin master

-Campo CSRF
Cada vez que defina un formulario HTML en su aplicación, debe incluir un campo de token CSRF 
oculto en el formulario para que el middleware de protección CSRF 
pueda validar la solicitud. Puede usar la @csrf directiva Blade para generar el campo de token

--Agregar los laravelCollective de laravel FORM ->composer require "laravelcollective/html":"^5.6.0" para laravel 5.8.36
