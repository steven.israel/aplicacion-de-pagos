const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

// grupo de estilos para el login
mix.styles([
    'resources/assets/js/plugins/nucleo/css/nucleo.css',
    'resources/assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css',
    'resources/assets/css/argon-dashboard.css'
], 'public/assets/css/login_css.css').version();

mix.scripts([
    'resources/assets/js/plugins/jquery/dist/jquery.min.js',
    'resources/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js',
    'resources/assets/js/argon-dashboard.min.js'
], 'public/assets/js/login_js.js').version();

mix.styles([
    'resources/template/vendors/bootstrap/dist/css/bootstrap.min.css',
    'resources/template/vendors/font-awesome/css/font-awesome.min.css',
    'resources/template/vendors/nprogress/nprogress.css',
    'resources/template/vendors/iCheck/skins/flat/green.css',
    'resources/template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
    'resources/template/vendors/jqvmap/dist/jqvmap.min.css',
    'resources/template/vendors/bootstrap-daterangepicker/daterangepicker.css',
    'resources/template/build/css/custom.min.css',
], 'public/assets/css/index_css.css').version();

mix.scripts([
   'resources/template/vendors/jquery/dist/jquery.min.js',
   'resources/template/vendors/bootstrap/dist/js/bootstrap.min.js',
   'resources/template/vendors/fastclick/lib/fastclick.js',
   'resources/template/vendors/nprogress/nprogress.js',
   'resources/template/vendors/Chart.js/dist/Chart.min.js',
   'resources/template/vendors/gauge.js/dist/gauge.min.js',
   'resources/template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
   'resources/template/vendors/iCheck/icheck.min.js',
   'resources/template/vendors/skycons/skycons.js',
   'resources/template/vendors/Flot/jquery.flot.js',
   'resources/template/vendors/Flot/jquery.flot.pie.js',
   'resources/template/vendors/Flot/jquery.flot.time.js',
   'resources/template/vendors/Flot/jquery.flot.stack.js',
   'resources/template/vendors/Flot/jquery.flot.resize.js',
   'resources/template/vendors/flot.orderbars/js/jquery.flot.orderBars.js',
   'resources/template/vendors/flot-spline/js/jquery.flot.spline.min.js',
   'resources/template/vendors/flot.curvedlines/curvedLines.js',
   'resources/template/vendors/DateJS/build/date.js',
   'resources/template/vendors/jqvmap/dist/jquery.vmap.js',
   'resources/template/vendors/jqvmap/dist/maps/jquery.vmap.world.js',
   'resources/template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js',
   'resources/template/vendors/moment/min/moment.min.js',
   'resources/template/vendors/bootstrap-daterangepicker/daterangepicker.js',
   'resources/template/build/js/custom.min.js'
], 'public/assets/js/index_js.js').version();