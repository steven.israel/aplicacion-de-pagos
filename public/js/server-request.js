/* 
 * Para manejar las peticiones al servidor
 */

/* 
 Created on : 14-nov-2015, 7:15:07
 Author     : Nestor Flores <nestorflores87@gmail.com>
 */

(function ($) {
    ///var _someLocalVar;
    $.server = function (url, params, fn, type, responseType, complete) {
        var t = (typeof type == 'undefined') ? 'post' : type;
        var r = (typeof reponseType == 'undefined') ? 'json' : responseType;

        $.ajax({
            url: url,
            data: params,
            type: t,
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status == 422) {
                    var title = 'Ocurrió un error de validación';
                    var message = $("<div/>");

                    var ul = $("<ul/>");
                    $.each(XMLHttpRequest.responseJSON, function (i, item) {
                        ul.append($("<li/>", {'text': item}));
                    });

                    message.append(ul);

                    toastr.error(message, title);
                } else if (XMLHttpRequest.status == 401) {
                    toastr.error('Al parecer no tiene permisos sobre esta operación, consulte al administrador', 'Acceso restringido');
                }else {
                    toastr.error('Ocurrió un error desconocido en la plataforma, trabajamos en arreglarlo', 'Error desconocido');
                }
                //alert('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
            },
            success: function (data) {
                if (typeof data.type != 'undefined') {
                    fn(data);
                } else {
                    toastr.error('Ocurrió un error desconocido en la plataforma, trabajamos en arreglarlo', 'Error desconocido');
                }
            },
            complete: complete,
            dataType: r
        });
    }
})(jQuery);




function procMsg(responseJSON) {
    if (responseJSON.type == 'error') {
        toastr.error(responseJSON.msg, responseJSON.title);
    } else if (responseJSON.type == 'warning') {
        toastr.warning(responseJSON.msg, responseJSON.title);
    } else if (responseJSON.type == 'success') {
        toastr.success(responseJSON.msg, responseJSON.title);
    } else {
        toastr.info(responseJSON.msg, responseJSON.title);
    }
}